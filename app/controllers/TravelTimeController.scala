package controllers

import java.time.ZoneId
import java.time.ZonedDateTime

import com.igeolise.traveltimesdk.ApiCredentials
import com.igeolise.traveltimesdk.DefaultBackend.backend
import com.igeolise.traveltimesdk.TravelTimeSDK
import com.igeolise.traveltimesdk.dto.common.Coords
import com.igeolise.traveltimesdk.dto.requests.RequestUtils.TravelTimePlatformResponse
import com.igeolise.traveltimesdk.dto.requests.common.CommonProperties
import com.igeolise.traveltimesdk.dto.requests.common.CommonProperties.PropertyType
import com.igeolise.traveltimesdk.dto.requests.common.Location
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.Bus
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.CommonTransportation
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.Cycling
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.CyclingPublicTransport
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.CyclingPublicTransportParams
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.Driving
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.DrivingTrain
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.DrivingTrainParams
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.PublicTransport
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.PublicTransportationParams
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.Train
import com.igeolise.traveltimesdk.dto.requests.common.Transportation.Walking
import com.igeolise.traveltimesdk.dto.requests.timefilter.TimeFilterRequest
import com.igeolise.traveltimesdk.dto.responses.common.DistanceBreakdown
import com.igeolise.traveltimesdk.dto.responses.timefilter.TimeFilterResponse
import guidle.Guidle
import javax.inject.Inject
import javax.inject.Singleton
import models.rest.Event
import models.rest.Response
import models.rest.TravelTimeInfo
import play.api.libs.json.Json.toJson
import play.api.mvc.AbstractController
import play.api.mvc.Action
import play.api.mvc.AnyContent
import play.api.mvc.ControllerComponents
import play.api.mvc.Result

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.duration.Duration.Zero
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.MINUTES

@Singleton
class TravelTimeController @Inject()(cc: ControllerComponents)
    extends AbstractController(cc) {
  import TravelTimeController._

  private val sdk = TravelTimeSDK.defaultSdk(
    ApiCredentials("6a0e6f98", "610304f6a47d6492ffe4c9d3b623d323"),
    backend
  )

  private def guidleLocations(
      evs: Array[Event] = Guidle.eventList
  ): Array[Location] = {
    evs.map { event =>
      Location(event.id.toString, Coords(event.latitude, event.longitude))
    }
  }

  def index(
      lat: Double,
      lng: Double,
      transportationMode: String,
      travelTime: Int
  ): Action[AnyContent] = process(lat, lng, transportationMode, travelTime)

  def fullIndex(
      lat: Double,
      lng: Double,
      transportationMode: String,
      travelTime: Int
  ): Action[AnyContent] = {
    process(lat, lng, transportationMode, travelTime, isFull = true)
  }

  def event(
      id: Long,
      lat: Double,
      lng: Double,
      travelTime: Int
  ): Action[AnyContent] = Action.async {
    Guidle.findById(id) match {
      case Some(e) =>
        val loc = Location(e.id.toString, Coords(e.latitude, e.longitude))

        val request = {
          def toDepartureSearch(locations: Array[Location]) = {
            TPModes.map { tp =>
              TimeFilterRequest.DepartureSearch(
                s"${tp}_${locations.head.id}",
                OriginId,
                locations.map(_.id),
                matchTransportationMode(tp),
                Duration(travelTime, MINUTES),
                ZonedDateTime.now(TimeZone),
                None,
                SearchProperties
              )
            }
          }

          TimeFilterRequest(
            Location(OriginId, Coords(lat, lng)) :: loc :: Nil,
            toDepartureSearch(Seq(loc).toArray),
            Nil
          )
        }

        sdk.send(request).map {
          case Left(e) =>
            BadRequest(s"Query TTP failed\n$e")
          case Right(ress) =>
            val z = ress.results
              .map(r => (r.searchId.takeWhile(_ != '_'), r.locations))
              .flatMap { case (mode, locations) => locations.map((_, mode)) }
              .groupBy { case (loc, _) => loc.id }

            val lel = z.flatMap {
              case (id, locxmode) =>
                Guidle.findById(id.toLong).map { ev =>
                  ev.copy(
                    ttps = Some(
                      locxmode
                        .map {
                          case (loc, mode) =>
                            val props = loc.properties.head
                            val travelTime =
                              props.travelTime.getOrElse(Zero).toMinutes
                            val breakdown =
                              props.distanceBreakdown
                                .map(_.parts)
                                .getOrElse(Nil)
                            models.rest.Event
                              .TTP(
                                mode,
                                footprint(breakdown),
                                travelTime,
                                fare(breakdown)
                              )
                        }
                        .sortBy(t => t.footprint / math.max(t.travelTime, 1))
                    )
                  )
                }
            }

            Ok(toJson(lel.head))
        }
      case None => Future.successful(BadRequest(""))
    }
  }

  def all(
      lat: Double,
      lng: Double,
      travelTime: Int,
      category: String
  ): Action[AnyContent] = Action.async {
    val gLocs = if (category.nonEmpty) {
      guidleLocations(
        Guidle.eventList
          .filter(_.category.toLowerCase().contains(category.toLowerCase()))
      )
    } else {
      guidleLocations()
    }

    val request = {
      def toDepartureSearch(locations: Array[Location]) = {
        TPModes.map { tp =>
          TimeFilterRequest.DepartureSearch(
            s"${tp}_${locations.head.id}",
            OriginId,
            locations.map(_.id),
            matchTransportationMode(tp),
            Duration(travelTime, MINUTES),
            ZonedDateTime.now(TimeZone),
            None,
            SearchProperties
          )
        }
      }

      TimeFilterRequest(
        Location(OriginId, Coords(lat, lng)) +: gLocs,
        gLocs.grouped(2000).flatMap(toDepartureSearch).toSeq,
        Nil
      )
    }

    sdk.send(request).map {
      case Left(e) =>
        BadRequest(s"Query TTP failed\n$e")
      case Right(ress) =>
        val z = ress.results
          .map(r => (r.searchId.takeWhile(_ != '_'), r.locations))
          .flatMap { case (mode, locations) => locations.map((_, mode)) }
          .groupBy { case (loc, _) => loc.id }

        val lel = z.flatMap {
          case (
              id: String,
              locxmode: Seq[(TimeFilterResponse.Location, String)]
              ) =>
            Guidle.findById(id.toLong).map { ev =>
              ev.copy(
                ttps = Some(
                  locxmode
                    .map {
                      case (loc, mode) =>
                        val props = loc.properties.head
                        val travelTime =
                          props.travelTime.getOrElse(Zero).toMinutes
                        val breakdown =
                          props.distanceBreakdown.map(_.parts).getOrElse(Nil)
                        models.rest.Event
                          .TTP(
                            mode,
                            footprint(breakdown),
                            travelTime,
                            fare(breakdown)
                          )
                    }
                    .sortBy(t => t.footprint / math.max(t.travelTime, 1))
                )
              )
            }
        }

        Ok(toJson(lel))
    }
  }

  def info(): Action[AnyContent] = Action {
    Ok(s"""{
          |"transportation_modes": [
          |  "pt", "bus", "train", "driving", "cycling", "walking", "cycling+pt", "driving+train"
          |]
          |}""".stripMargin)
  }

  def process(
      lat: Double,
      lng: Double,
      transportationMode: String,
      travelTime: Int,
      isFull: Boolean = false
  ): Action[AnyContent] = Action.async {
    val request = makeRequest(
      Coords(lat, lng),
      matchTransportationMode(transportationMode),
      Duration(travelTime, MINUTES)
    )

    sdk.send(request).map {
      case Left(e)               => BadRequest(s"Query TTP failed\n$e")
      case Right(res) if !isFull => makeResponse1(res)
      case Right(res) if isFull  => makeResponse2(res)
    }
  }

  private def makeRequest(
      latLng: Coords,
      transportationMode: CommonTransportation,
      travelTime: FiniteDuration
  ): TimeFilterRequest = {
    def toDepartureSearch(locations: Array[Location]) = {
      TimeFilterRequest.DepartureSearch(
        locations.head.id,
        OriginId,
        locations.map(_.id),
        transportationMode,
        travelTime,
        ZonedDateTime.now(TimeZone),
        None,
        SearchProperties
      )
    }

    TimeFilterRequest(
      Location(OriginId, latLng) +: guidleLocations(),
      guidleLocations().grouped(2000).map(toDepartureSearch).toSeq,
      Nil
    )
  }

  private def makeResponse1(ttpResult: TravelTimePlatformResponse): Result = {
    ttpResult match {
      case TimeFilterResponse(results, _) =>
        val infos = results.flatMap(_.locations).map { loc =>
          val props      = loc.properties.head
          val travelTime = props.travelTime.getOrElse(Zero).toMinutes
          val breakdown  = props.distanceBreakdown.map(_.parts).getOrElse(Nil)
          TravelTimeInfo(loc.id.toLong, travelTime.toInt, footprint(breakdown))
        }

        Ok(toJson(infos))
      case _ =>
        InternalServerError("Bad response from TTP")
    }
  }

  private def makeResponse2(ttpResult: TravelTimePlatformResponse): Result = {
    ttpResult match {
      case TimeFilterResponse(results, _) =>
        val infos = results.flatMap(_.locations).map { loc =>
          val props      = loc.properties.head
          val travelTime = props.travelTime.getOrElse(Zero).toMinutes
          val breakdown  = props.distanceBreakdown.map(_.parts).getOrElse(Nil)
          TravelTimeInfo(loc.id.toLong, travelTime.toInt, footprint(breakdown))
        }

        val fullInfos = infos.map { info =>
          Response(Guidle.findById(info.id).get.simple, info)
        }

        Ok(toJson(fullInfos))
      case _ =>
        InternalServerError("Bad response from TTP")
    }
  }

  private def footprint(
      breakdown: Seq[DistanceBreakdown.BreakdownPart]
  ): Int = {
    // https://www.bbc.com/news/science-environment-49349566
    val total = breakdown.map { part =>
      part.distanceMeters * (part.mode match {
        case "car" | "parking" | "plane" => 0.120
        case "train" | "rail_national" | "rail_overground" |
            "rail_underground" | "rail_dlr" =>
          0.041
        case "coach" => 0.027
        case "bus"   => 0.104
        case _       => 0
      })
    }.sum

    total.toInt
  }

  private def fare(
      breakdown: Seq[DistanceBreakdown.BreakdownPart]
  ): Int = {
    val total = breakdown.map { part =>
      part.distanceMeters * (part.mode match {
        case "car" | "parking" | "plane" => 0.0002
        case "train" | "rail_national" | "rail_overground" |
            "rail_underground" | "rail_dlr" =>
          0.0004
        case "coach" | "bus" => 0.00015
        case _               => 0
      })
    }.sum

    total.toInt
  }
}

object TravelTimeController {
  val OriginId: String = "origin"
  val TimeZone: ZoneId = ZoneId.of("Europe/Zurich")

  val SearchProperties: Seq[CommonProperties.TimeFilterRequestProperty] = Seq(
    PropertyType.TravelTime,
    PropertyType.DistanceBreakdown
  )

  val TPModes: List[String] = List("pt", "driving", "cycling+pt")

  def matchTransportationMode(mode: String): CommonTransportation = mode match {
    case "pt"      => PublicTransport(PublicTransportationParams()) // OK
    case "bus"     => Bus(PublicTransportationParams())
    case "train"   => Train(PublicTransportationParams()) // Maybe
    case "driving" => Driving // OK
    case "cycling" => Cycling
    case "walking" => Walking
    case "cycling+pt" =>
      CyclingPublicTransport(CyclingPublicTransportParams()) // OK
    case "driving+train" => DrivingTrain(DrivingTrainParams())
    case _               => PublicTransport(PublicTransportationParams())
  }
}
