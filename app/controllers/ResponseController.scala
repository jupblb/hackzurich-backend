package controllers

import javax.inject.Inject
import play.api.mvc.AbstractController
import play.api.mvc.Action
import play.api.mvc.AnyContent
import play.api.mvc.ControllerComponents

class ResponseController @Inject()(cc: ControllerComponents, ttc: TravelTimeController)
    extends AbstractController(cc) {
  def index(
      lat: Double,
      lng: Double,
      transportationMode: String,
      travelTime: Int
  ): Action[AnyContent] = {
    ttc.process(lat, lng, transportationMode, travelTime, isFull = true)
  }
}
