package controllers

import guidle.Guidle
import javax.inject._
import play.api.libs.json.Json.toJson
import play.api.mvc._

@Singleton
class GuidleController @Inject()(cc: ControllerComponents)
    extends AbstractController(cc) {
  def index(page: Int, itemsPerPage: Int): Action[AnyContent] = Action {
    val begin  = page * itemsPerPage
    val end    = (page + 1) * itemsPerPage
    val events = Guidle.eventList.slice(begin, end)
    Ok(toJson(events))
  }

  def info: Action[AnyContent] = Action {
    Ok(s"""{
          |  "size": ${Guidle.eventSize},
          |  "categories": [${Guidle.flatCategories
            .map(c => s"""\"${c.title}\"""")
            .mkString(", ")}]
          |}""".stripMargin)
  }

  def event(id: Long): Action[AnyContent] = Action {
    Guidle.findById(id) match {
      case Some(e) => Ok(toJson(e))
      case None    => BadRequest("")
    }
  }
}
