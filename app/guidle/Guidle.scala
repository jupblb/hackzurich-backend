package guidle

import java.time.LocalDateTime

import models.guidle.Category
import models.guidle.Event
import models.guidle.EventCategory
import models.rest
import play.api.Logging
import play.api.libs.json.Json.parse

import scala.io.Source.fromFile

object Guidle extends Logging {
  lazy val eventList: Array[rest.Event] = {
    def loop(categoryId: Long): Option[Category] = {
      categories.find(_.id == categoryId).flatMap { category =>
        category.parentId match {
          case Some(parentId) => loop(parentId)
          case None           => Some(category)
        }
      }
    }

    val eventArr = events.map { event =>
      val category = eventCategories.find(_.eventId == event.id).flatMap {
        eventCategory =>
          loop(eventCategory.categoryId).map(_.title)
      }

      event.out(category.mkString)
    }

    eventArr.flatten
  }

  def flatCategories: Array[Category] = {
    categories.filter(_.parentId.isEmpty)
  }

  def eventSize: Int = eventList.length

  def findById(id: Long): Option[rest.Event] = eventList.find(_.id == id)

  private lazy val categories: Array[Category] = {
    val file = fromFile("data/guidle/category.json", "UTF-8").mkString

    parse(file).validate[Array[Category]].getOrElse {
      logger.error("Unable to read categories!")
      Array.empty[Category]
    }.map { cat =>
      if(cat.title.contains("Concert") || cat.title.contains("concert")) {
        cat.copy(parentId = Some(Int.MaxValue))
      } else {
        cat
      }
    } :+ Category(Int.MaxValue, "Concert", None)
  }

  private lazy val events: Array[Event] = {
    val file = fromFile("data/guidle/event.json", "UTF-8").mkString

    val events = parse(file).validate[Array[Event]].getOrElse {
      logger.error("Unable to read events!")
      Array.empty[Event]
    }

    val now = LocalDateTime.now()

    events.filter(_.startDate match {
      case Some(date) => date.isBefore(now.plusDays(1))
      case None       => true
    })
  }

  private lazy val eventCategories: Array[EventCategory] = {
    val file = fromFile("data/guidle/event_category.json", "UTF-8").mkString

    parse(file).validate[Array[EventCategory]].getOrElse {
      logger.error("Unable to read event ~ category!")
      Array.empty[EventCategory]
    }
  }
}
