package models.guidle

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ofPattern

import models.rest
import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath
import play.api.libs.json.Reads

case class Event(
    id: Long,
    day: String,
    startTime: Option[String],
    endTime: Option[String],
    title: String,
    shortDescription: Option[String],
    longDescription: Option[String],
    website: Option[String],
    price: Option[String],
    thumbnail: Option[String],
    street: Option[String],
    addressLine: Option[String],
    zip: Option[String],
    city: Option[String],
    country: Option[String],
    venueName: Option[String],
    latitude: Option[Double],
    longitude: Option[Double]
) {
  def startDate: Option[LocalDateTime] = startTime.map(date)
  def endDate: Option[LocalDateTime]   = endTime.map(date)

  def out(categoryName: String): Option[rest.Event] = {
    val event = for {
      startDate <- startDate
      endDate   <- endDate
      lat       <- latitude
      lng       <- longitude
    } yield rest.Event(
      id,
      startDate.toString,
      endDate.toString,
      title,
      categoryName,
      shortDescription,
      longDescription,
      thumbnail,
      price,
      street,
      addressLine,
      zip,
      city,
      country,
      venueName,
      lat,
      lng
    )

    event.foreach(_.startDate = startDate)

    event
  }

  private def date(hour: String): LocalDateTime = {
    LocalDateTime.from(Event.DateFormat.parse(s"$day ${hour.dropRight(2)}"))
  }
}

object Event {
  val DateFormat: DateTimeFormatter = ofPattern("yyyy-MM-dd HH:mm:ss")

  implicit val eventReads: Reads[Event] = (
    (JsPath \ "event_id").read[Long] and
      (JsPath \ "date").read[String] and
      (JsPath \ "start_time").readNullable[String] and
      (JsPath \ "end_time").readNullable[String] and
      (JsPath \ "title_en").read[String] and
      (JsPath \ "short_description_en").readNullable[String] and
      (JsPath \ "long_description_en").readNullable[String] and
      (JsPath \ "homepage").readNullable[String] and
      (JsPath \ "price_information").readNullable[String] and
      (JsPath \ "thumbnail_url").readNullable[String] and
      (JsPath \ "address_street").readNullable[String] and
      (JsPath \ "address_address_line").readNullable[String] and
      (JsPath \ "address_zip").readNullable[String] and
      (JsPath \ "address_city").readNullable[String] and
      (JsPath \ "address_country").readNullable[String] and
      (JsPath \ "address_venue_name").readNullable[String] and
      (JsPath \ "address_latitude").readNullable[Double] and
      (JsPath \ "address_longitude").readNullable[Double]
    )(Event.apply _)
}
