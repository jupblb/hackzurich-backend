package models.guidle

import play.api.libs.json.Reads
import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath

case class EventCategory(eventId: Long, categoryId: Long)

object EventCategory {
  implicit val eventCategoryReads: Reads[EventCategory] = (
    (JsPath \ "event_id").read[Long] and (JsPath \ "category_id").read[Long]
  )(EventCategory.apply _)
}
