package models.guidle

import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath
import play.api.libs.json.Reads

case class Category(id: Long, title: String, parentId: Option[Long])

object Category {
  implicit val categoryReads: Reads[Category] = (
    (JsPath \ "category_id").read[Long] and
      (JsPath \ "title_en").read[String] and
      (JsPath \ "parent_category_id").readNullable[Long]
    )(Category.apply _)
}
