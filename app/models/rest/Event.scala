package models.rest

import java.time.LocalDate
import java.time.LocalDateTime

import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath
import play.api.libs.json.Writes

case class Event(
    id: Long,
    start: String,
    end: String,
    title: String,
    category: String,
    shortDescription: Option[String],
    longDescription: Option[String],
    thumbnail: Option[String],
    price: Option[String],
    street: Option[String],
    addressLine: Option[String],
    zip: Option[String],
    city: Option[String],
    country: Option[String],
    venueName: Option[String],
    latitude: Double,
    longitude: Double,
    ttps: Option[Seq[Event.TTP]] = None
) {
  def simple: Event = this.copy(longDescription = None)
  var startDate: Option[LocalDateTime] = None
}

object Event {
  implicit val ttpWrites: Writes[TTP] = (
    (JsPath \ "mode").write[String] and
      (JsPath \ "carbon_footprint").write[Int] and
      (JsPath \ "travel_time").write[Long] and
      (JsPath \ "cost").write[Int]
  )(unlift(TTP.unapply))

  implicit val eventWrites: Writes[Event] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "start_date").write[String] and
      (JsPath \ "end_date").write[String] and
      (JsPath \ "title").write[String] and
      (JsPath \ "category").write[String] and
      (JsPath \ "short_description").writeNullable[String] and
      (JsPath \ "long_description").writeNullable[String] and
      (JsPath \ "thumbnail").writeNullable[String] and
      (JsPath \ "price_information").writeNullable[String] and
      (JsPath \ "address_street").writeNullable[String] and
      (JsPath \ "address_address_line").writeNullable[String] and
      (JsPath \ "address_zip").writeNullable[String] and
      (JsPath \ "address_city").writeNullable[String] and
      (JsPath \ "address_country").writeNullable[String] and
      (JsPath \ "address_venue_name").writeNullable[String] and
      (JsPath \ "address_latitude").write[Double] and
      (JsPath \ "address_longitude").write[Double] and
      (JsPath \ "ttp").writeNullable[Seq[Event.TTP]]
    )(unlift(Event.unapply))

  case class TTP(mode: String, footprint: Int, travelTime: Long, fare: Int)
}
