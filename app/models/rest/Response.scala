package models.rest

import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath
import play.api.libs.json.Writes

case class Response(event: Event, travelTimeInfo: TravelTimeInfo)

object Response {
  implicit val responseWrites: Writes[Response] = (
    (JsPath \ "event").write[Event] and (JsPath \ "ttp").write[TravelTimeInfo]
  )(unlift(Response.unapply))
}