package models.rest

import play.api.libs.functional.syntax._
import play.api.libs.json.JsPath
import play.api.libs.json.Writes

case class TravelTimeInfo(
    id: Long,
    travelTime: Int,
    footprint: Int
)

object TravelTimeInfo {
  implicit val travelTimeInfoWrites: Writes[TravelTimeInfo] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "travel_time").write[Int] and
      (JsPath \ "carbon_footprint").write[Int]
    )(unlift(TravelTimeInfo.unapply))
}