name := """mapup"""
organization := "com.mapup"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.10"

libraryDependencies ++= Seq(
  guice,
  "com.igeolise" %% "traveltime-platform-sdk" % "1.6.0"
)

resolvers += Resolver.bintrayRepo("igeolise", "maven")

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.mapup.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.mapup.binders._"
